$(document).ready(function(){
    
    var wow = new WOW(
        {
          boxClass:     'wow',      // animated element css class (default is wow)
          animateClass: 'animated', // animation css class (default is animated)
          offset:       0,          // distance to the element when triggering the animation (default is 0)
          mobile:       true,       // trigger animations on mobile devices (default is true)
          live:         true,       // act on asynchronously loaded content (default is true)
          callback:     function(box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
          },
          scrollContainer: null,    // optional scroll container selector, otherwise use window,
          resetAnimation: true,     // reset animation on end (default is true)
        }
      );
      wow.init();

    $("#formLp").validate({
        rules: {
            nombres: "required",
            telefono: {
                required: true,
                minlength: 8,
                maxlength: 12,
                number:true
            },
            correo: {
                required: true,
                email: true
            },
            empresa: "required",
            fecha: "required",

            terminos: {
                required: true
            }
        },
        messages:{
            nombres: "Ingrese su nombre por favor.",
            telefono: "Ingrese su teléfono por favor.",
            correo: "Ingrese su correo por favor.",
            empresa: "Ingrese su empresa por favor.",
            fecha: "Seleccione una fecha por favor.",
            terminos: "Acepte los TÉRMINOS Y CONDICIONES.",
        },
        submitHandler: function(){
            window.location.href = "/gracias.html";
        }
    });
});